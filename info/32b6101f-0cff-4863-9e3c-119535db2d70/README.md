# Malware ZYX

El malware ZYX es una pieza de software malicioso que se propaga a través de redes vulnerables y que afecta principalmente a dispositivos con arquitectura MIPS.
Funcionamiento

## Funcionamiento

Este malware se propaga mediante la explotación de vulnerabilidades en sistemas Zyxel, permitiendo al atacante ejecutar comandos de forma remota en el dispositivo afectado. Una vez que el malware se ha propagado, se conecta a un servidor remoto para descargar y ejecutar un archivo binario llamado "zyx.mips" y "zyx.mipsel".

El archivo "zyx.mips" y "zyx.mipsel" es un ejecutable ELF (Executable and Linkable Format) que incluye varios payloads que permiten al atacante tomar el control del dispositivo infectado. El malware es capaz de realizar una amplia variedad de actividades maliciosas, como el robo de información, la creación de puertas traseras, la realización de ataques DDoS, entre otras.

## IoC

El malware ZYX se puede detectar utilizando el hash SHA1 "edaf554a684f2e7b291443ffdb56901f1aaaf7b0". Además, el malware contiene la cadena "zyx" en su nombre y su código fuente, lo que puede ser utilizado como un indicador de compromiso (IoC) para detectar la presencia del malware en un sistema.
Detección y mitigación

Se recomienda a los usuarios proteger sus dispositivos manteniendo los sistemas y software actualizados, así como utilizando contraseñas seguras y no predeterminadas. También se recomienda el uso de soluciones de seguridad como antivirus y firewalls para detectar y bloquear la propagación del malware.
