# COVID-19.exe

## Resumen

El malware analizado es una variante de ransomware que infecta el Master Boot Record (MBR) del equipo y busca inutilizarlo para pedir un rescate para desbloquearlo. Los desarrolladores de malware están aprovechando la pandemia COVID-19 para inspirar sus proyectos. El binario objeto de análisis es un ejecutable para sistemas Windows de 32-bits con la firma SHA256: COVID-19.exe dfbcce38214fdde0b8c80771cfdec499fc086735c8e7e25293e7292fc7993b4c. 

## Forma de Infección

 El ataque se lleva a cabo mediante el uso excesivo de herramientas de línea de comando para alterar los datos del registro o archivo, la ejecución de archivos batch y scripts de Visual Basic. También se utilizan técnicas de evasión de seguridad, como la creación de procesos en modo suspendido y la carga de DLLs faltantes, así como la infección del código bootmgr y sector de arranque del disco duro. 

## Forma de trabajar en la máquina

 El malware utiliza un componente dropper que se encarga de extraer y ejecutar varios archivos en el equipo infectado. El componente dropper requiere privilegios de administrador para su ejecución y, una vez lanzado el archivo batch "coronavirus.bat", ya no forma parte del proceso de infección. Este archivo batch se encarga de crear un directorio de instalación, modificar valores en el registro del sistema y asegurar la persistencia de los binarios extraídos previamente. También deshabilita algunas funciones del sistema, cambia el fondo de pantalla, y reinicia el equipo para garantizar que los binarios se ejecuten al iniciar el sistema. 

## Indicadores de compromiso

| Fichero | SHA256 |
|---------|--------|
| COVID-19.exe | dfbcce38214fdde0b8c80771cfdec499fc086735c8e7e25293e7292fc7993b4c |
| coronavirus.bat | 4fd9b85eec0b49548c462acb9ec831a0728c0ef9e3de70e772755834e38aa3b3 |
| end.exe | c3f11936fe43d62982160a876cc000f906cb34bb589f4e76e54d0a5589b2fdb9 |
| mainWindow.exe | b780e24e14885c6ab836aae84747aa0d975017f5fc5b7f031d51c7469793eabe |
| run.exe | c46c3d2bea1e42b628d6988063d247918f3f8b69b5a1c376028a2a0cadd53986 |
| Update.vbs | a1a8d79508173cf16353e31a236d4a211bdcedef53791acce3cfba600b51aaec |
| run.bat | df1f9777fe6bede9871e331c76286bab82da361b59e44d07c6d977319522ba91 |

| Directorio de instalación |
|------|
| %homedrive%\COVID-19 |
| %homedrive%\COVID-19\Update.vbs |
| %homedrive%\COVID-19\wallpaper.jpg |
| %homedrive%\COVID-19\cursor.cur |
| %homedrive%\COVID-19\end.exe |
| %homedrive%\COVID-19\mainWindow.exe |
| %homedrive%\COVID-19\run.exe |

| Clave de registro | Valor de la clave |
|--|--|
| HKCU\Software\Microsoft\Windows\CurrentVersion\Policies\System\wallpaper | %homedrive%\COVID-19\wallpaper.jpg |
| HKLM\Software\Microsoft\Windows\CurrentVersion\Run\CheckForUpdates | %homedrive%\COVID-19\Update.vbs |
| HKLM\Software\Microsoft\Windows\CurrentVersion\Run\explorer.exe | %homedrive%\COVID-19\run.exe |
| HKLM\software\Microsoft\Windows\CurrentVersion\Run\GoodbyePC! | %homedrive%\COVID-19\end.exe |
| HKLM\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Run\CheckForUpdates | %homedrive%\COVID-19\Update.vbs |
| HKLM\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Run\explorer.exe | %homedrive%\COVID-19\run.exe |
| HKLM\software\Wow6432Node\Microsoft\Windows\CurrentVersion\Run\GoodbyePC! | %homedrive%\COVID-19\end.exe |
